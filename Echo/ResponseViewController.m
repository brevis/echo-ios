//
//  ResponseViewController.m
//  Echo
//
//  Created by brevis on 12/3/14.
//  Copyright (c) 2014 brevis. All rights reserved.
//

#import "ResponseViewController.h"
#import "Echo.h"

@implementation ResponseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [NSFetchedResultsController deleteCacheWithName:[self cacheName]];
}

- (NSPredicate*)filterPredicate {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"packetType = %d", ECHO_PACKET_TYPE_RESPONSE];
    return predicate;
}

- (NSString*)cacheName {
    return @"EchoResponse";
}

- (UITableView*)tableView {
    return self.responseTableView;
}

@end
