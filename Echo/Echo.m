//
//  Echo.m
//  Echo
//
//  Created by brevis on 12/4/14.
//  Copyright (c) 2014 brevis. All rights reserved.
//

#import "Echo.h"


@implementation Echo

@dynamic bodyText;
@dynamic packetType;
@dynamic format;
@dynamic booleanField;
@dynamic status;
@dynamic createdDate;

@end
