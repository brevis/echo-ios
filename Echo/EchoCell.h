//
//  EchoCell.h
//  Echo
//
//  Created by brevis on 12/10/14.
//  Copyright (c) 2014 brevis. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EchoCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *bodyTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *detailsLabel;

@end
