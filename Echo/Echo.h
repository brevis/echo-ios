//
//  Echo.h
//  Echo
//
//  Created by brevis on 12/4/14.
//  Copyright (c) 2014 brevis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

#define ECHO_PACKET_TYPE_REQUEST 0
#define ECHO_PACKET_TYPE_RESPONSE 1

#define ECHO_FORMAT_XML 0
#define ECHO_FORMAT_JSON 1
#define ECHO_FORMAT_BINARY 2

#define ECHO_STATUS_WAITING 0
#define ECHO_STATUS_SENT 1
#define ECHO_STATUS_RECEIVED 2

@interface Echo : NSManagedObject

@property (nonatomic, retain) NSString * bodyText;
@property (nonatomic, retain) NSNumber * packetType;
@property (nonatomic, retain) NSNumber * format;
@property (nonatomic, retain) NSNumber * booleanField;
@property (nonatomic, retain) NSNumber * status;
@property (nonatomic, retain) NSDate * createdDate;

@end
