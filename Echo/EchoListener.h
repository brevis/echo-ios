//
//  EchoListener.h
//  Echo
//
//  Created by brevis on 12/9/14.
//  Copyright (c) 2014 brevis. All rights reserved.
//

#import <Foundation/Foundation.h>

#define ECHO_SAVE_LISTENER @"ECHO_SAVE_LISTENER"

@class EchoController;

@interface EchoListener : NSObject

@property (readwrite) EchoController *echoController;

- (void)echoSaveHandler:(NSNotification*) notification;

@end
