//
//  ControlsViewController.m
//  Echo
//
//  Created by brevis on 12/3/14.
//  Copyright (c) 2014 brevis. All rights reserved.
//

#import "ControlsViewController.h"
#import "EchoController.h"

@implementation ControlsViewController

@synthesize logTextView, connectionStatusLabel, bodyTextField, booleanFieldSwitcher, formatSegmentControl, sendButton;
@synthesize echoController;

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

- (IBAction)sendButtonTapped:(UIButton *)button {
    
    if ( [self.bodyTextField.text isEqualToString:@""] ) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Enter message" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    
    [self.echoController saveEcho];
    
    [self.bodyTextField setText:@""];
}

@end
