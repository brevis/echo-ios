//
//  ControlsViewController.h
//  Echo
//
//  Created by brevis on 12/3/14.
//  Copyright (c) 2014 brevis. All rights reserved.
//

#import <UIKit/UIKit.h>

@class EchoController;

@interface ControlsViewController : UIViewController

@property (readwrite) EchoController *echoController;

@property (weak, nonatomic) IBOutlet UITextView *logTextView;
@property (weak, nonatomic) IBOutlet UILabel *connectionStatusLabel;
@property (weak, nonatomic) IBOutlet UITextField *bodyTextField;
@property (weak, nonatomic) IBOutlet UISwitch *booleanFieldSwitcher;
@property (weak, nonatomic) IBOutlet UISegmentedControl *formatSegmentControl;
@property (weak, nonatomic) IBOutlet UIButton *sendButton;

- (IBAction)sendButtonTapped:(UIButton *)button;

@end
