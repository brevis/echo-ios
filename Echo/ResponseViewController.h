//
//  ResponseViewController.h
//  Echo
//
//  Created by brevis on 12/3/14.
//  Copyright (c) 2014 brevis. All rights reserved.
//

#import "RequestViewController.h"

@interface ResponseViewController : RequestViewController

@property (strong, nonatomic) IBOutlet UITableView *responseTableView;

@end
