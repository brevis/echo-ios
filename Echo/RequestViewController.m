//
//  RequestViewController.m
//  Echo
//
//  Created by brevis on 12/3/14.
//  Copyright (c) 2014 brevis. All rights reserved.
//

#import "RequestViewController.h"
#import "Echo.h"
#import "EchoController.h"
#import "Packet.h"
#import "EchoCell.h"

@implementation RequestViewController

@synthesize echoController;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    id delegate = [[UIApplication sharedApplication] delegate];
    self.managedObjectContext = [delegate managedObjectContext];
        
    [NSFetchedResultsController deleteCacheWithName:[self cacheName]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSPredicate*)filterPredicate {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"packetType = %d", ECHO_PACKET_TYPE_REQUEST];
    return predicate;
}

- (NSString*)cacheName {
    return @"EchoRequest";
}

- (UITableView*)tableView {
    return self.requestTableView;
}

#pragma mark - TableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    EchoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (void)configureCell:(EchoCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    Echo *echo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    //cell.textLabel.text = [NSString stringWithFormat:@"Status: %@, Format: %@, BooleanField: %@, Message: %@", echo.status, echo.format, echo.booleanField, echo.bodyText];

    cell.bodyTextLabel.text = echo.bodyText;
    
    NSString *status = @"";
    switch ([echo.status integerValue]) {
        case ECHO_STATUS_RECEIVED:
            status = @"Received";
            break;
        
        case ECHO_STATUS_WAITING:
            status = @"Waiting...";
            break;

        case ECHO_STATUS_SENT:
            status = @"Sent";
            break;
    }
    
    NSString *format = @"";
    switch ([echo.format integerValue]) {
        case ECHO_FORMAT_BINARY:
            format = @"BIN";
            break;
            
        case ECHO_FORMAT_JSON:
            format = @"JSON";
            break;
            
        case ECHO_FORMAT_XML:
            format = @"XML";
            break;
    }
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"dd/MM HH:mm:ss";
    NSString *date = [dateFormatter stringFromDate:echo.createdDate];
    
    cell.detailsLabel.text = [NSString stringWithFormat:@"%@ | %@ | %@", date, status, format];    
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 52;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 52;
}

#pragma mark - Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController {    
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Echo" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    // predicate
    [fetchRequest setPredicate:[self filterPredicate]];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:20];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"createdDate" ascending:YES];
    NSArray *sortDescriptors = @[sortDescriptor];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:[self cacheName]];
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [[self tableView] beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller
   didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath
     forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
        [[self tableView] insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
        break;

        case NSFetchedResultsChangeUpdate:
        [self configureCell:[[self tableView] cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
        break;
    }
    
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [[self tableView] endUpdates];
}


@end
