//
//  Packet.m
//  Echo
//
//  Created by brevis on 12/9/14.
//  Copyright (c) 2014 brevis. All rights reserved.
//

#import "Packet.h"
#import "Echo.h"

@implementation Packet

+ (Packet*)makeFromEcho:(Echo *) echo {
    Packet *packet = [[Packet alloc] initWithBodyText:echo.bodyText booleanField:(Boolean)echo.booleanField format:echo.format objectId:echo.objectID.URIRepresentation.absoluteString];
    return packet;
}

+ (Packet*)makeFromSerializedData:(id) data {
    Packet *packet = [[Packet alloc] initWithRawData:data];
    if ( [data isKindOfClass:[NSString class]] ) {
        NSString *stringData = (NSString *)data;
        if ([stringData hasPrefix:@"<?xml"]) {
            [packet unserializeFromXml];
        } else if ([stringData hasPrefix:@"{"] && [stringData hasSuffix:@"}"]) {
            [packet unserializeFromJson];
        }
    } else {
        [packet unserializeFromBinary];
    }
    return packet;
}

- (id)initWithBodyText:(NSString*) bodyText booleanField:(Boolean) booleanField format:(NSNumber*) format objectId:(NSString*) objectId {
    if (self = [super init]) {
        _bodyText = bodyText;
        _booleanField = booleanField;
        _format = format;
        _objectId = objectId;
    }
    return self;
}

- (id)initWithRawData:(id) data {
    if (self = [super init]) {
        _rawData = (NSData*)data;
    }
    return self;
}

- (NSString*)objectId {
    return _objectId;
}

- (id)serialize {    
    
    switch ([_format longLongValue]) {
        case ECHO_FORMAT_XML:
            return [self serializeToXml];
            break;
        
        case ECHO_FORMAT_JSON:
            return [self serializeToJson];
            break;

        case ECHO_FORMAT_BINARY:
            return [self serializeToBinary];
            break;
    }
    return nil;
}

- (NSString*)serializeToXml {
    NSMutableData *archiveData = [NSMutableData data];
    NSKeyedArchiver *archiver = [[NSKeyedArchiver alloc] initForWritingWithMutableData:archiveData];
    archiver.outputFormat = NSPropertyListXMLFormat_v1_0;
    [archiver encodeRootObject:[self makeLightweightObject]];
    [archiver finishEncoding];
    return [[NSString alloc] initWithData:archiveData encoding:NSUTF8StringEncoding];
}

- (NSString*)serializeToJson {
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:[self makeLightweightObject] options:0 error:&error];
    return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
}

- (NSData*)serializeToBinary {
    NSMutableData *archiveData = [NSMutableData data];
    NSKeyedArchiver *archiver = [[NSKeyedArchiver alloc] initForWritingWithMutableData:archiveData];
    archiver.outputFormat = NSPropertyListXMLFormat_v1_0;
    [archiver encodeRootObject:[self makeLightweightObject]];
    [archiver finishEncoding];
    return archiveData;
}

- (void)unserializeFromXml {
    _format = [NSNumber numberWithLong:ECHO_FORMAT_XML];
    
    NSData *xmlData = [(NSString*)_rawData dataUsingEncoding:NSUTF8StringEncoding];
    NSKeyedUnarchiver *unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData:xmlData];
    [self assignUnserializedFields:(NSDictionary*)[unarchiver decodeObject]];
}

- (void)unserializeFromJson {
    _format = [NSNumber numberWithLong:ECHO_FORMAT_JSON];
    NSError *error;
    NSData *jsonData = [(NSString*)_rawData dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *data = (NSDictionary*)[NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&error];
    if(error == nil) {
        [self assignUnserializedFields:data];
    } else {
        NSLog(@"Error unserialize JSON object: %@ %@", error, [error userInfo]);
    }
}

- (void)unserializeFromBinary {
    _format = [NSNumber numberWithLong:ECHO_FORMAT_BINARY];
    
    NSKeyedUnarchiver *unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData:_rawData];
    [self assignUnserializedFields:(NSDictionary*)[unarchiver decodeObject]];
}

-(void)assignUnserializedFields:(NSDictionary*) fields {
    if (fields != nil){
        _bodyText = [fields valueForKey:@"bodyText"];
        _booleanField = (Boolean)[fields valueForKey:@"booleanField"];
        _objectId = [fields valueForKey:@"objectId"];
    }
}

- (NSDictionary*)makeLightweightObject {
    return [NSDictionary dictionaryWithObjectsAndKeys:
            _bodyText, @"bodyText",
            [NSNumber numberWithBool:_booleanField], @"booleanField",
            _objectId, @"objectId",
            nil];
}

@end
