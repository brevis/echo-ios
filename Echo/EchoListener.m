//
//  EchoListener.m
//  Echo
//
//  Created by brevis on 12/9/14.
//  Copyright (c) 2014 brevis. All rights reserved.
//

#import "EchoListener.h"
#import "Echo.h"
#import "Packet.h"
#import "EchoController.h"

@implementation EchoListener

@synthesize echoController;

- (id)init {
    if (self = [super init]) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(echoSaveHandler:) name:ECHO_SAVE_LISTENER object:nil];
    }
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:ECHO_SAVE_LISTENER object:nil];
}

- (void)echoSaveHandler:(NSNotification*) notification {
    // send packet after 3 seconds delay
    // just for Echo status not changed instantly (Waiting -> Sent)
    
    double delayInSeconds = 3.0;
    dispatch_time_t time = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(time, dispatch_get_main_queue(), ^(void){
        dispatch_async(dispatch_get_main_queue(), ^(void){
            Echo *echo = (Echo*)notification.object;
            [self.echoController sendPacket:[Packet makeFromEcho:echo]];
            echo.status = [NSNumber numberWithInt:ECHO_STATUS_SENT];
            NSManagedObjectContext *context = [self.echoController managedObjectContext];
            NSError *error = nil;
            if ( ![context save:&error] ) {
                [self.echoController addLogMessage:[NSString stringWithFormat:@"Error update request object: %@ %@", error, [error userInfo]]];
            } else {
                [self.echoController addLogMessage:[NSString stringWithFormat:@"Updated request object with ID=%@", [echo objectID]]];
            }
        });
    });
}

@end
