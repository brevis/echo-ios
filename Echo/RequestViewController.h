//
//  RequestViewController.h
//  Echo
//
//  Created by brevis on 12/3/14.
//  Copyright (c) 2014 brevis. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@class EchoController;

@interface RequestViewController : UIViewController <UITableViewDataSource,UITableViewDelegate, NSFetchedResultsControllerDelegate>

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;

@property (readwrite) EchoController *echoController;

@property (weak, nonatomic) IBOutlet UITableView *requestTableView;

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;

@end
