//
//  EchoController.m
//  Echo
//
//  Created by brevis on 12/3/14.
//  Copyright (c) 2014 brevis. All rights reserved.
//

#import "EchoController.h"
#import "ControlsViewController.h"
#import "RequestViewController.h"
#import "ResponseViewController.h"
#import "Echo.h"

@implementation EchoController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    id delegate = [[UIApplication sharedApplication] delegate];
    self.managedObjectContext = [delegate managedObjectContext];
    
    // ControlsViewController
    self.controlsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ControlsViewController"];
    self.controlsViewController.view.frame = self.controlsView.bounds;
    [self.controlsView addSubview:self.controlsViewController.view];
    [self addChildViewController:self.controlsViewController];
    [self.controlsViewController didMoveToParentViewController:self];
    [self.controlsViewController setEchoController:self];
    
    // RequestViewController
    self.requestViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"RequestViewController"];
    self.requestViewController.view.frame = self.requestView.bounds;
    [self.requestView addSubview:self.requestViewController.view];
    [self addChildViewController:self.requestViewController];
    [self.requestViewController didMoveToParentViewController:self];
    [self.requestViewController setEchoController:self];
    
    // ResponseViewController
    self.responseViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ResponseViewController"];
    self.responseViewController.view.frame = self.responseView.bounds;
    [self.responseView addSubview:self.responseViewController.view];
    [self addChildViewController:self.responseViewController];
    [self.responseViewController didMoveToParentViewController:self];
    [self.responseViewController setEchoController:self];

    // Websocket
    isConnected = NO;
    [self connect];
    
    // EchoListener
    self.echoListener = [[EchoListener alloc] init];
    self.echoListener.echoController = self;
}

- (void)viewDidUnload {
    [self.webSocket close];
}

#pragma mark - API

- (void)addLogMessage:(NSString*) message {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *date = [dateFormatter stringFromDate:[[NSDate alloc] init]];
    [self.controlsViewController.logTextView setText:[NSString stringWithFormat:@"%@[%@] %@\n\n", self.controlsViewController.logTextView.text, date, message]];
}

- (void)setConnectionStatus:(Boolean) status {
    isConnected = status;
    if ( status == YES ) {
        self.controlsViewController.connectionStatusLabel.text = @"Connected";
        self.controlsViewController.connectionStatusLabel.textColor = [UIColor greenColor];
        [self addLogMessage:@"Connected"];
    } else {
        self.controlsViewController.connectionStatusLabel.text = @"Offline";
        self.controlsViewController.connectionStatusLabel.textColor = [UIColor blackColor];
        [self addLogMessage:@"Offline"];
    }
}

- (void)saveEcho {
    NSManagedObjectContext *context = [self managedObjectContext];
    NSManagedObject *managedObject = [NSEntityDescription insertNewObjectForEntityForName:@"Echo" inManagedObjectContext:context];
    
    [managedObject setValue:self.controlsViewController.bodyTextField.text forKey:@"bodyText"];
    [managedObject setValue:[NSNumber numberWithBool:self.controlsViewController.booleanFieldSwitcher.on] forKey:@"booleanField"];
    [managedObject setValue:[NSNumber numberWithInteger:self.controlsViewController.formatSegmentControl.selectedSegmentIndex] forKey:@"format"];
    [managedObject setValue:[NSNumber numberWithInteger:ECHO_PACKET_TYPE_REQUEST] forKey:@"packetType"];
    [managedObject setValue:[NSNumber numberWithInteger:ECHO_STATUS_WAITING] forKey:@"status"];
    [managedObject setValue:[NSDate date] forKey:@"createdDate"];
    
    NSError *error = nil;
    if ( [context save:&error] ) {
        [self addLogMessage:[NSString stringWithFormat:@"Saved request object with ID=%@", [managedObject objectID]]];
        [[NSNotificationCenter defaultCenter] postNotificationName:ECHO_SAVE_LISTENER object:(Echo*)managedObject];
    } else {
        [self addLogMessage:[NSString stringWithFormat:@"Error save request object: %@ %@", error, [error userInfo]]];
    }
}

- (NSManagedObject*)makeResponseObjectFrom:(NSManagedObject*) object withContext:(NSManagedObjectContext*) context {
    NSManagedObject *managedObject = [NSEntityDescription insertNewObjectForEntityForName:@"Echo" inManagedObjectContext:context];
    
    [managedObject setValue:[object valueForKey:@"bodyText"]
                     forKey:@"bodyText"];
    [managedObject setValue:[object valueForKey:@"booleanField"]
                     forKey:@"booleanField"];
    [managedObject setValue:[object valueForKey:@"format"]
                     forKey:@"format"];
    [managedObject setValue:[NSNumber numberWithInteger:ECHO_PACKET_TYPE_RESPONSE]
                     forKey:@"packetType"];
    [managedObject setValue:[NSNumber numberWithInteger:ECHO_STATUS_RECEIVED]
                     forKey:@"status"];
    [managedObject setValue:[NSDate date]
                     forKey:@"createdDate"];
    
    return managedObject;
}

#pragma mark - Network

- (void)connect{
    NSURL *url = [[NSURL alloc] initWithString:@"ws://echo.websocket.org"];
    
    self.webSocket = [[SRWebSocket alloc] initWithURL:url];
    self.webSocket.delegate = self;
    [self.webSocket open];
}

- (void)sendPacket:(Packet *)packet {
    if (isConnected) {
        NSData *data = [packet serialize];
        [self.webSocket send:data];
        [self addLogMessage:[NSString stringWithFormat:@"Sent packet: %@", data]];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Offline" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
}

#pragma mark - SRWebSocketDelegate

- (void)webSocket:(SRWebSocket *)webSocket didReceiveMessage:(id)message{
    Packet *packet = [Packet makeFromSerializedData:message];
    
    [self addLogMessage:[NSString stringWithFormat:@"Received packet: %@", message]];
    
    if ([packet objectId] != nil) {    
        NSURL *url = [[NSURL alloc] initWithString:[packet objectId]];
        NSManagedObjectID *objectId = [[[self managedObjectContext] persistentStoreCoordinator] managedObjectIDForURIRepresentation:url];
        NSManagedObjectContext *context = [self managedObjectContext];
        NSManagedObject *echo = [context objectWithID:objectId];
        if (echo != nil) {
            NSManagedObject *receivedEcho = [self makeResponseObjectFrom:echo withContext:context];
            NSError *error = nil;
            if ([context save:&error]) {
                [self addLogMessage:[NSString stringWithFormat:@"Saved received object with ID=%@", [receivedEcho objectID]]];
            } else {
                [self addLogMessage:[NSString stringWithFormat:@"Error save received object: %@ %@", error, [error userInfo]]];
            }
        } else {
            [self addLogMessage:[NSString stringWithFormat:@"Can't get Echo object with ID=%@ from DB", objectId]];
        }
    } else {
        [self addLogMessage:[NSString stringWithFormat:@"Can't process received packet: after unserialization objectId is null"]];
    }
}

- (void)webSocketDidOpen:(SRWebSocket *)webSocket {
    [self setConnectionStatus:YES];
}

- (void)webSocket:(SRWebSocket *)webSocket didFailWithError:(NSError *)error {
    [self addLogMessage:[NSString stringWithFormat:@"Socket error: %@", error]];
    [self setConnectionStatus:NO];

    // try reconnect
    [self.webSocket close];
    double delayInSeconds = 5.0;
    dispatch_time_t time = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(time, dispatch_get_main_queue(), ^(void){
        dispatch_async(dispatch_get_main_queue(), ^(void){
            [self connect];
        });
    });
}

- (void)webSocket:(SRWebSocket *)webSocket didCloseWithCode:(NSInteger)code reason:(NSString *)reason wasClean:(BOOL)wasClean {
    [self setConnectionStatus:NO];
}

@end
