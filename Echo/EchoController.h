//
//  EchoController.h
//  Echo
//
//  Created by brevis on 12/3/14.
//  Copyright (c) 2014 brevis. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ControlsViewController.h"
#import "RequestViewController.h"
#import "ResponseViewController.h"
#import <SocketRocket/SRWebSocket.h>
#import "Packet.h"
#import "EchoListener.h"

@interface EchoController : UIViewController <SRWebSocketDelegate, NSFetchedResultsControllerDelegate> {
    Boolean isConnected;
}

@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;

@property (readwrite) ControlsViewController *controlsViewController;
@property (readwrite) RequestViewController *requestViewController;
@property (readwrite) ResponseViewController *responseViewController;
@property (readwrite) SRWebSocket *webSocket;
@property (readwrite) EchoListener *echoListener;

@property (weak, nonatomic) IBOutlet UIView *controlsView;
@property (weak, nonatomic) IBOutlet UIView *requestView;
@property (weak, nonatomic) IBOutlet UIView *responseView;

- (void)addLogMessage:(NSString*) message;
- (void)setConnectionStatus:(Boolean) status;
- (void)saveEcho;
- (void)connect;
- (void)sendPacket:(Packet *) packet;
- (NSManagedObject*)makeResponseObjectFrom:(NSManagedObject*) object withContext:(NSManagedObjectContext*) context;

@end

