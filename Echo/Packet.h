//
//  Packet.h
//  Echo
//
//  Created by brevis on 12/9/14.
//  Copyright (c) 2014 brevis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Echo.h"

@interface Packet : NSObject {
    NSString *_objectId;
    NSString *_bodyText;
    Boolean _booleanField;
    NSNumber *_format;
    NSData *_rawData;
}

+ (Packet*)makeFromEcho:(Echo *)echo;
+ (Packet*)makeFromSerializedData:(id) data;
- (NSString*)objectId;
- (id)serialize;

@end
